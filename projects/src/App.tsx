import styles from "./App.module.scss";
import Accordian from "./components/accordian";
import LoadMoreData from "./components/load-more-button";
import QRCodeGenerator from "./components/qr-code-generator";
import RandomColor from "./components/random-color";
import StarRating from "./components/star-rating";
import TreeView from "./components/tree-view";
import menus from "./components/tree-view/data";

function App() {
  return (
    <div className={styles.App}>

      {/*Accordian Component*/}
      {/* <Accordian /> */}

      {/* Randon Color Component */}
      {/* <RandomColor /> */}

      {/* Star rating Component */}
      {/* <StarRating noOfStars={10}/> */}

      {/* Image Slider Component */}
      {/* <ImageSlider url={} limit={}/> */}

      {/* Load more products component */}
      {/* <LoadMoreData /> */}

      {/* Tree View Component */}
      {/* <TreeView menus={menus} /> */}

      {/* QR Code Generator */}
      <QRCodeGenerator />

    </div>
  )
}

export default App
