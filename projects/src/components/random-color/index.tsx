import { useEffect, useState } from "react"
import styles from './styles.module.scss';


const RandomColor = () => {

  const [typeOfColor, setTypeOfColor] = useState('hex');
  const [color, setColor] = useState('#000000');

  function RandomColorUtility(length: number) {
    return Math.floor(Math.random()*length);
  }

  function handleCreateRandomColor() {
    const hex = [1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];
    let hexColor = '#';

    for(let i=0; i<6; i++){
      hexColor += hex[RandomColorUtility(hex.length)]
    }

    console.log(hexColor);
    setColor(hexColor);
  }


  function handleCreateRGBColor() {
    const r = RandomColorUtility(256)
    const g = RandomColorUtility(256)
    const b = RandomColorUtility(256)

    setColor(`rgb(${r}, ${g}, ${b})`);
  }

  useEffect(() => {
    if(typeOfColor === 'rgb') handleCreateRGBColor();
    else handleCreateRandomColor();
  }, [typeOfColor]);

  return (
    <div 
      style={
        {
          width: "100vw",
          height: "100vh",
          background: color,
        }
      }
      >
      <button onClick={() => setTypeOfColor('hex')}>Create HEX color</button>
      <button onClick={() => setTypeOfColor('rgb')}>Create RGB color</button>
      <button onClick={typeOfColor === 'hex' ? handleCreateRandomColor : handleCreateRGBColor }>Generate Random Color</button>

      <div style={
        {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          color: 'wheat',
          fontSize: '2rem',
          marginTop: '6rem',
          flexDirection: 'column',
          gap: '3rem'
        }
      }>
        <h3>{typeOfColor === 'rgb' ? 'RGB Color' : 'HEX Color'}</h3>
        <h1>{color}</h1>
      </div>

    </div>
  )
}

export default RandomColor