import styles from './styles.module.scss'
import menus from './data';
import MenuItem from './menuitem';

const MenuList = ({list = []}) => {
  return (
    <ul className={styles.menuListContainer}>
      {
        list && list.length ?
        list.map((listItem) => <MenuList item={listItem} />)
        : null
      }
    </ul>
  )
}

export default MenuList;