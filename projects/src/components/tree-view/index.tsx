import styles from './styles.module.scss'
import MenuList from './menulist';
import menus from './data';

const TreeView = ({menus: any = []}) => {

  return (
    <div className={styles.treeViewContainer}>

    <MenuList list={menus} />
    </div>
  )
}

export default TreeView;