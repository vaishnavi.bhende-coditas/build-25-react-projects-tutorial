import styles from "./styles.module.scss";
import data from "./data";

// There are two types of accordian
// 1. Single selection
// 2. Multiple selection

// >>>>>>>>>  Normal way

// export default function Accordian() {
//   return <div>Accordian</div>
// }

//  Single Selection >>>>

import { useState } from "react";

export default function Accordian() {
  const [selected, setSelected] = useState(null);
  const [enableMultiSelection, setenableMultiSelection] = useState(false);
  const [multiple, setMultiple] = useState([]);

  function handleSingleSelection(getCurrentId: any) {
    // console.log(getCurrentId);
    setSelected(getCurrentId === selected ? null : getCurrentId);
  }

  function handleMultiSelection(getCurrentId: any) {
    let cpyMultiple = [...multiple];
    const findIndexOfCurrentId = cpyMultiple.indexOf(getCurrentId);

    console.log(findIndexOfCurrentId);
    if (findIndexOfCurrentId === -1) cpyMultiple.push(getCurrentId);
    else cpyMultiple.splice(findIndexOfCurrentId, 1);

    setMultiple(cpyMultiple);
  }

  console.log(selected);

  return (
    <div className={styles.wrapper}>
      <button onClick={() => setenableMultiSelection(!enableMultiSelection)}>
        Enable Multiple Selection
      </button>
      <div className={styles.accordian}>
        {data && data.length > 0 ? (
          data.map((dataItem) => (
            <div className={styles.item}>
              <div
                onClick={
                  enableMultiSelection
                    ? () => handleMultiSelection(dataItem.id)
                    : () => handleSingleSelection(dataItem.id)
                }
                className={styles.title}
              >
                <h3>{dataItem.question}</h3>
                <span>+</span>
              </div>
              {enableMultiSelection
                ? multiple.indexOf(dataItem.id) !== -1 && (
                    <div className={styles.content}>{dataItem.answer}</div>
                  )
                : selected === dataItem.id && (
                    <div className={styles.content}>{dataItem.answer}</div>
                  )}

              {/* {
                selected === dataItem.id || multiple.indexOf(dataItem.id) !== -1 ?
                (<div className={styles.content}>{dataItem.answer}</div>)
                : null
              } */}
            </div>
          ))
        ) : (
          <div>No data found!</div>
        )}
      </div>
    </div>
  );
}
