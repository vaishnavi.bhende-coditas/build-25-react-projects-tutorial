import { useState } from "react";
import style from "./style.module.scss";
import QRCode from "react-qr-code";

const QRCodeGenerator = () => {

  const [qrCode, setQrCode] = useState('');
  const [input, setInput] = useState('')

  function handleGenerateQrCode() {
    setQrCode(input)
    setInput('')
  }

  return (
    <>
      <div className={style.container}>
        <h1>QR Code Generator</h1>
        <div>
          <input onChange={(e) => setInput(e.target.value)} 
          type="text" 
          name="QR-Code" 
          value={input}
          placeholder="Enter your value"/>

          <button disabled={input && input.trim() !== "" ? false : true} onClick={handleGenerateQrCode}>Generate</button>
        </div>
        <div>
          <QRCode 
            id = "qr code value"
            value= {qrCode}
            size={400}
            bgColor="#fff"
          />
        </div>
      </div>
    </>
  )
}

export default QRCodeGenerator;