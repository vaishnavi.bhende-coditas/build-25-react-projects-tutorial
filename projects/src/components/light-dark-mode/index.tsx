import style from "./style.module.scss";
import useLocalStorage from "./useLocalStorage";

export interface Theme {
  light: string;
  dark: string;
}

const LightDarkMode = () => {

  const [theme, setTheme] = useLocalStorage<Theme>(
    'theme',
    { light: 'light', dark: 'dark' } // Default theme object
  );

  const handleToggleTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'lght')
  }

  console.log(theme);

  return (
    <div className={style.lightDarkMode}>
      <div className={style.container}>
        <p>Hello World!!!</p>
        <button onClick={handleToggleTheme}>Change Theme</button>
      </div>
    </div>
  )
}

export default LightDarkMode;