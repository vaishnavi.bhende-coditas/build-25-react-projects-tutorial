
export interface ImageSliderProps {
  url: string;
  limit: string;
}