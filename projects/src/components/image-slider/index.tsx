import { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import {ImageSliderProps} from './types';

const ImageSlider = (props: ImageSliderProps) => {

  const [images, setImages] = useState([]);
  const [currentSlide, setCurrentSlide] = useState(0);
  const [errorMsg, setErrorMsg] = useState(null);
  const [loading, setLoading] = useState(false)

  async function fetchImages(getUrl: any) {
     try {
        setLoading(true)

        const response = await fetch(`${getUrl}?`);
        const data = await response.json();

        if(data) {
          setImages(data)
          setLoading(false)
        }

     } catch(e: any) {
        setErrorMsg(e.message);
        setLoading(false)
     }
  }

  useEffect(() => {
    if(url !== '') fetchImages(url)
  }, [url])

  if(loading) {
    return <div>loading data ! Please wait</div>
  }

  if(errorMsg !== null) {
    return <div>Error occured ! {errorMsg}</div>
  }

  return (
    <div className={styles.container}>

    </div>
  )
}

export default ImageSlider;