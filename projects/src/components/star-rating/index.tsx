import { FaStar } from 'react-icons/fa';
import styles from './styles.module.scss';
import { useState } from 'react';

const StarRating = ({noOfStars = 5}) => {

  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(0);

  function handleClick(getCurrentIndex: any) {
    console.log(getCurrentIndex);
    setRating(getCurrentIndex);
  }

  function handleMouseEnter(getCurrentIndex: any) {
    console.log(getCurrentIndex);
    setHover(getCurrentIndex);
  }

  function handleMouseLeave() {
    // console.log(getCurrentIndex);
    setHover(rating);
  }

  return (
    <div className={styles.starRating}>
      {
        [...Array(noOfStars)].map((_, index) => {
          index += 1;

          return (
          <FaStar
          key = {index}
              className={`${index <= (hover || rating) ? styles.active : styles.inactive}`}
          onClick = {() => handleClick(index)}
          onMouseMove = {() => handleMouseEnter(index)}
          onMouseLeave = {() => handleMouseLeave()}
          size={40}
          />
          )
        })
      }
    </div>
  )
}

export default StarRating;